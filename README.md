# home-manager-config

My personal home-manager configuration.

## Installation

### Pre-requisites

- Nix =< 2.4
- An x86 Linux system or Nix-on-droid,

### Basic

To perform a basic, non-full installation, run

```bash
nix --experimental-features "nix-command flakes" run gitlab:TheOPtimal/home-manager-config#desktop-minimal --impure
```

### Complete

To perform a full installation, run

```bash
git clone https://gitlab.com/TheOPtimal/home-manager-config $HOME/.config/nixpkgs/
cd $HOME/.config/nixpkgs
nix run .#desktop-minimal
```

## Tweaking

To be added
