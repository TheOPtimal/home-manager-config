# home-manager-config
# Copyright (C) 2021  Jacob Gogichaishvili
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
_: let
  packages = import ./packages;
  modules = import ./module-lists;
in {
  imports = [
    packages.desktop
		# packages.extra
    # packages.gaming
    packages.terminal

    modules.desktop
    modules.cli
  ];

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";
}
