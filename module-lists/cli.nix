{...}: {
  imports = [
    ../applications/doom-emacs
    ../applications/nix
    ../applications/zsh
    ../modules/applications/aria2.nix
    ../modules/applications/bat.nix
    ../modules/applications/direnv.nix
    ../modules/applications/git.nix
    ../modules/applications/helix.nix
    ../modules/applications/man.nix
    ../modules/applications/skim.nix
    ../modules/applications/ssh.nix
    ../modules/applications/zellij.nix
    ../modules/base16.nix
    ../modules/defaults.nix
    ../modules/email.nix
    ../modules/home.nix
  ];
}
