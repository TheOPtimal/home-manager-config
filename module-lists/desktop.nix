{...}: {
  imports = [
    ../applications/firefox
    ../modules/applications/alacritty.nix
    ../modules/applications/mpv.nix
    ../modules/applications/syncthing.nix
    ../modules/dconf
    ../modules/fonts.nix
    ../modules/gnome-extensions
    ../modules/gtk.nix
    ../modules/mangohud.nix
    ../modules/qt.nix
    ../modules/xresources.nix
    ../templates
  ];
}
