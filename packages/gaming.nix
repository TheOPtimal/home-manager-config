{
  pkgs,
  prismlauncher,
  ...
}: {
  home.packages = with pkgs; [
    steam
    prismlauncher
    openrgb
  ];
}
