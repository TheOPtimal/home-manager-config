{pkgs, ...}: {
  home.packages = with pkgs; [
    zotero
    prusa-slicer
    wireshark
    gqrx
    logseq

    icon-library
    mousai
    nicotine-plus

    # Media Productivity
    gimp-with-plugins
    inkscape
    krita
    xournalpp
    rnote
    blender
    kdenlive
  ];
}
