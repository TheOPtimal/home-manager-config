{pkgs, ...}: {
  home.packages = with pkgs; [
    exa
    gitui
    ripgrep
    ripgrep-all
    tealdeer
    fd
    sd
    bottom
    thefuck
    du-dust
  ];
}
