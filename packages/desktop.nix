{
  pkgs,
  opkgs,
  ...
}: {
  home.packages = with pkgs;
  with opkgs; [
    fragments
    amberol
    qalculate-gtk
    pika-backup
    clapper
    drawing
    eyedropper
    shortwave
  ];
}
