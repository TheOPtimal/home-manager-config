# home-manager-config
# Copyright (C) 2021  Jacob Gogichaishvili
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
{
  description = "TheOPtimal's home-manager configurations";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";

    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        utils.follows = "flake-utils";
      };
    };

    opkgs = {
      url = "gitlab:TheOPtimal/opkgs";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    nix-doom-emacs = {
      url = "github:nix-community/nix-doom-emacs";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    prismlauncher.url = "github:PrismLauncher/PrismLauncher";

    # Firefox userChrome.css
    firefox-gnome-theme = {
      url = "github:rafaelmardojai/firefox-gnome-theme";
      flake = false;
    };

    # base16 related
    base16 = {
      url = "github:SenchoPens/base16.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    base16-nord = {
      url = "github:spejamchr/base16-nord-scheme";
      flake = false;
    };

    base16-shell = {
      url = "github:tinted-theming/base16-shell";
      flake = false;
    };

    base16-helix = {
      url = "github:tinted-theming/base16-helix";
      flake = false;
    };
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
    home-manager,
    opkgs,
    prismlauncher,
    base16,
    nix-doom-emacs,
    ...
  } @ inputs:
    flake-utils.lib.eachDefaultSystem (system: let
      # Nixpkgs
      pkgs = nixpkgs.legacyPackages.${system};
      # Opkgs
      opkg = opkgs.legacyPackages.${system};
      # Home manager packages
      hm-packages = home-manager.packages.${system};

      # Extra arguments provided to the modules
      extraSpecialArgs =
        inputs
        // {
          inherit (prismlauncher.packages.${system}) prismlauncher;
          opkgs = opkg;
          olib = opkgs.lib;
        };
      # Out-of-tree modules
      extraModules = [base16.homeManagerModule nix-doom-emacs.hmModule];
    in rec {
      homeManagerConfigurations = let
        # Simple function to reduce boilerplate
        mkBasicConfig = attrs:
          home-manager.lib.homeManagerConfiguration ({
              inherit pkgs system extraSpecialArgs extraModules;
            }
            // attrs);

        # This expands on the above function but is purpose-built for desktops only.
        mkDesktopConfig = configPath:
          mkBasicConfig {
            configuration = configPath;
            username = "optimal";
            homeDirectory = "/home/optimal";
          };
        # Map `mkDesktopConfig` to each imported confguration
      in
        (builtins.mapAttrs (_: mkDesktopConfig) self.hmConfigsRaw)
        // {
          # Add extra configuration for nix-on-droid as it has different requirements
          nix-on-droid = mkBasicConfig {
            configuration = ./home-cli.nix;
            username = "nix-on-droid";
            homeDirectory = "/data/data/com.termux.nix/files/home";
          };
        };

      # NixOS modules to easily add these configs to a NixOS system
      nixosModules = let
        # This function generates a NixOS module that realizes the configuration.
        mkHomeModule = config: {
          home-manager = {
            # useGlobalPkgs = true;
            useUserPackages = true;
            users.optimal = config;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
            inherit extraSpecialArgs;
            sharedModules = extraModules;
          };
        };
        # This generates a NixOS module for each imported home-manager configuration
      in
        builtins.mapAttrs (_: mkHomeModule) self.hmConfigsRaw;

      devShell = pkgs.mkShell {
        packages = [
          hm-packages.home-manager
          opkg.p-codestyle
        ];
      };

      # Activation packages, running one applies the home-manager configuration. An activationPackage is mapped to each home-manager configuration.
      packages = builtins.mapAttrs (_: config: config.activationPackage) homeManagerConfigurations;
      apps =
        builtins.mapAttrs (_: pkg: {
          type = "app";
          program = "${pkg}/activate";
        })
        packages;
    })
    // {
      # Raw imports of the configurations
      hmConfigsRaw = {
        desktop = import ./home-desktop.nix;
        desktop-minimal = import ./home-desktop-minimal.nix;
        cli = import ./home-cli.nix;
      };

      overlays = import ./overlays.nix;
    };
}
