{
  shellAliases = {
    c = "clear"; # Simple alias for clear
    mv = "noglob zmv -W"; # Better mv
    cp = "cp -i"; # Confirm before overwriting something
    df = "df -h"; # Human-readable sizes
    du = "du -h"; # Human-readable sizes
    free = "free -m"; # Show sizes in MB
    ls = "exa --icons"; # Better LS
    la = "ls -a"; # Alias for seeing hiddenfiles
    ll = "ls -l"; # Alias for detailed file listing
    lt = "ls -T"; # Tree-style
    le = "ls -@"; # Alias for seeing extended file attributes
    l = "ls -la@"; # D) All of the above

    fzf = "sk"; # sk is better and faster than fzf.

    # Git aliases
    ga = "git add";
    gaa = "git add .";
    gc = "git commit -m";
    gp = "git push";
    gpo = "git push origin";
    gpom = "git push origin master";
  };
  shellGlobalAliases = {
    # Folder aliases
    "..." = "../..";
    "...." = "../../..";
    "....." = "../../../..";
    "......" = "../../../../..";
    "......." = "../../../../../..";
    "........" = "../../../../../../..";
    "........." = "../../../../../../../..";
    ".........." = "../../../../../../../../..";
    "..........." = "../../../../../../../../../..";
  };
}
