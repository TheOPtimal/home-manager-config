{
  config,
  olib,
  base16-shell,
  ...
}: {
  imports = [
    ./starship.nix
    ./zoxide.nix
  ];

  programs.zsh =
    {
      enable = true;
      enableAutosuggestions = true;
      enableCompletion = true;
      enableSyntaxHighlighting = true;
      # Integrate with VTE-based terminal
      enableVteIntegration = true;
      # Allow changing to a folder without the cd command
      autocd = true;
      # Use vi-based keybindings
      defaultKeymap = "viins";
      # dotDir = ".config/zsh";

      history = {
        # Expire duplicate commands in the history first
        expireDuplicatesFirst = true;
        # Share history between sessions
        share = true;
      };

      # Load base16 theme and configuration
      initExtra = olib.concatFiles [(config.scheme base16-shell) ./configuration.zsh];

      zplug = {
        enable = true;
        plugins = [
          # Adds handy `clipcopy` and `clippaste` commands
          {
            name = "lib/clipboard";
            tags = ["from:oh-my-zsh"];
          }
          # Better completion
          {
            name = "lib/completion";
            tags = ["from:oh-my-zsh"];
          }
          # History substring search
          {
            name = "zsh-users/zsh-history-substring-search";
          }
        ];
      };
    }
    // (import ./aliases.nix); # Add aliases

  # Enable command-not-found
  programs.command-not-found.enable = true;
}
