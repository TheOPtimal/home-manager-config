{config, ...}: {
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      add_newline = true;
      shlvl = {
        disabled = false;
        threshold = 3;
      };
      directory.substitutions = {
        "/run/media/${config.home.username}" = "External";
        "/run/media/${config.home.username}/OPUSB/School/Grade 8" = "School";
      };
    };
  };
}
