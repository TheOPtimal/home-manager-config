eval `thefuck -a`

# Setopt
setopt interactive_comments

# Autoload
unalias run-help
autoload -U colors zcalc zmv run-help
colors

# Window Title
function set_win_title(){
    echo -ne "\033]0; $(pwd | sed "s:$HOME:~:g") \007"
}
