_: {
  programs.doom-emacs = {
    enable = true;
    doomPrivateDir = ./.;
  };
}
