;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Jacob Gogichaishvili"
      user-mail-address "iakob.gogichaishvili@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq doom-font (font-spec :family "FiraCode Nerd Font" :size 15 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "ETBembo" :size 19))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-nord)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


;; Fonts

(use-package! org
  :config

  (setq org-hide-emphasis-markers t)
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  (let* ((title            `(:height 2.0))

         (base-font-color  (face-foreground 'default  nil 'default))
         (primary-color    (face-foreground 'mode-line nil))
         (secondary-color  (face-background 'secondary-selection nil 'region))
         (background-color (face-background 'default nil 'default))
         (org-highlights   `(:foreground ,base-font-color :background ,secondary-color :height 0.8)))

    (custom-theme-set-faces
     'user
     ;; `(outline-8          ((t (:inherit outline-8))))
     ;; `(outline-7          ((t (:inherit outline-7))))
     ;; `(outline-6          ((t (:inherit outline-6))))
     ;; `(outline-5          ((t (:inherit outline-5))))
     `(outline-4          ((t (:height 1.1))))
     `(outline-3          ((t (:height 1.25))))
     `(outline-2          ((t (:height 1.5))))
     `(outline-1          ((t (:height 1.75))))
     `(org-document-info  ((t (:inherit default   :height 1.35))))
     `(org-roam-title     ((t ,title)))
     `(org-document-title ((t ,title)))

     `(org-block            ((t (:foreground ,base-font-color :background ,background-color :box nil))))
     `(org-block-begin-line ((t ,org-highlights)))
     `(org-block-end-line   ((t ,org-highlights)))

     `(org-checkbox ((t (:foreground "#000000", :background "#93a1a1" :box (:line-width -3 :color "#93a1a1" :style "released-button")))))

     `(org-headline-done ((t (:strike-through t))))
     `(org-done          ((t (:strike-through t))))))

  (add-hook 'org-mode-hook 'variable-pitch-mode)

  (custom-theme-set-faces
   'user
   '(line-number                  ((t (:inherit fixed-pitch))))
   '(line-number-current-line     ((t (:inherit fixed-pitch))))
   '(org-block                    ((t (:inherit fixed-pitch))))
   '(org-code                     ((t (:inherit (shadow fixed-pitch)))))
   '(org-quote                    ((t (:inherit default))))
   '(org-verse                    ((t (:inherit default))))
   '(org-document-info-keyword    ((t (:inherit (shadow fixed-pitch)))))
   '(org-indent                   ((t (:inherit (org-hide fixed-pitch)))))
   '(org-link                     ((t (:foreground "royal blue" :underline t))))
   '(org-meta-line                ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-property-value           ((t (:inherit fixed-pitch))) t)
   '(org-special-keyword          ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-table                    ((t (:inherit fixed-pitch :foreground "#83a598"))))
   '(org-tag                      ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
   '(org-verbatim                 ((t (:inherit (shadow fixed-pitch))))))

  (add-hook 'org-mode-hook 'visual-line-mode)

  (setq use-default-font-for-symbols nil)

  (defun my-emoji-fonts ()
    (set-fontset-font t 'unicode (face-attribute 'default :family))
    (set-fontset-font t '(#x2300 . #x27e7) "Twemoji")
    (set-fontset-font t '(#x2300 . #x27e7) "Noto Color Emoji" nil 'append)
    (set-fontset-font t '(#x27F0 . #x1FAFF) "Twemoji")
    (set-fontset-font t '(#x27F0 . #x1FAFF) "Noto Color Emoji" nil 'append))

  (if (daemonp)
      (add-hook 'server-after-make-frame-hook #'my-emoji-fonts)
    (my-emoji-fonts)))

;; Ement.el

(use-package! ement
  :commands ement-connect
  :config
  (setq ement-initial-sync-timeout 100)) ;; I have a very large Matrix account

;; Mastodon

(use-package! mastodon
  :commands mastodon
  :config
  (setq mastodon-instance-url "https://calckey.social"
        mastodon-active-user "optimal"))

;; mini-frame

(use-package! mini-frame
  :init
  (mini-frame-mode)

  :config
  (custom-set-variables
   '(mini-frame-show-parameters
     '((top . 175)
       (width . 0.75)
       (left . 0.5)))))
