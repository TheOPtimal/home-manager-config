{config, ...}: {
  # Configure nixpkgs internally
  nixpkgs = {
    config = import ./config.nix;
  };

  # Add symlinks and configure nix and nixpkgs externally
  xdg.configFile = {
    "nixpkgs/config.nix".source = ./config.nix;
    "nix/nix.conf".source = ./nix.conf;
  };
}
