{config, ...}: {
  gtk = {
    enable = true;

    font = {
      name = config.fonts.default.font;
      inherit (config.fonts.default) package;
      size = config.fonts.size.body;
    };

    theme = {
      name = "Adwaita-dark";
      package = gnome.gnome-themes-extra;
    };
  };
}
