{
  config,
  pkgs,
  lib,
  ...
}: {
  qt = {
    enable = true;
    # Theme Qt like GTK
    platformTheme = "gnome";
    style = {
      name = lib.toLower config.gtk.theme.name;
      package = pkgs.adwaita-qt;
    };
  };
}
