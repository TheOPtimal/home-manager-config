_: {
  programs.man = {
    enable = true;
    # Generate caches so that man starts faster
    generateCaches = true;
  };

  # Change the man pager to bat
  home.sessionVariables.MANPAGER = "sh -c 'col -bx | bat -l man -p'";
}
