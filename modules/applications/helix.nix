{
	config,
	base16-helix
}: {
	programs.helix = {
		enable = true;

		settings.editor = {
			line-number = "relative";
			bufferline = "multiple";
			cursorline = true;
			mouse = false;

			cursor-shape = {
				insert = "bar";
				normal = "block";
				select = "underline";
			};

			statusline = {
				left = ["mode" "spinner"];
				center = ["file-name"];
				right = ["diagnostics" "selections" "position" "file-encoding" "file-line-ending" "file-type"];
				separator = "│";
				mode.normal = "NORMAL";
				mode.insert = "INSERT";
				mode.select = "SELECT";
			};

			lsp.display_messages = true;
			file-picker.hidden = false;
		};

		settings.theme = "base16";

		themes.base16 = builtins.fromTOML ( builtins.readFile ( config.scheme base16-helix ) );
	};
}
