{
  config,
  opkgs,
  ...
}: {
  programs.alacritty = {
    enable = true;
    package = opkgs.alacritty;
    settings = {
      # Theming
      colors = with config.scheme.withHashtag; {
        # Background opacity
        # opacity = 1.00;

        # Default colors
        primary = {
          background = base00;
          foreground = base05;
        };

        # Colors the cursor will use if `custom_cursor_colors` is true
        cursor = {
          text = base00;
          cursor = base05;
        };

        # Normal colors
        normal = {
          black = base00;
          red = base08;
          green = base0B;
          yellow = base0A;
          blue = base0D;
          magenta = base0E;
          cyan = base0C;
          white = base05;
        };

        # Bright colors
        bright = {
          black = base03;
          red = base08;
          green = base0B;
          yellow = base0A;
          blue = base0D;
          magenta = base0E;
          cyan = base0C;
          white = base07;
        };

        indexed_colors = [
          {
            index = 16;
            color = base09;
          }
          {
            index = 17;
            color = base0F;
          }
          {
            index = 18;
            color = base01;
          }
          {
            index = 19;
            color = base02;
          }
          {
            index = 20;
            color = base04;
          }
          {
            index = 21;
            color = base06;
          }
        ];
      };

      cursor.style = "beam";

      font = with config.fonts; {
        normal.family = monospace.font;
        size = size.monospace;
        ligatures = true;
      };
    };
  };
}
