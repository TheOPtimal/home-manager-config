{config, ...}: {
  programs.bat = {
    enable = true;
    config = {
      # Theme with base16
      theme = "base16-256";
      # Use `less` as pager
      pager = "less -FR";
    };
  };
}
