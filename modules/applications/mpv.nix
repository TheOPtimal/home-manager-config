{
  config,
  pkgs,
  ...
}: {
  programs.mpv = {
    enable = true;
    # Use GPU acceleration
    defaultProfiles = ["gpu-hq"];
    config = {
      # Save screenshots to ~/Pictures
      screenshot-directory = builtins.toString ~/Pictures/Screenshots;
      # Disable OSC to make the thumbnail work
      osc = false;
      # Don't require the window to keep its aspect ratio
      keepaspect-window = false;
      # I have no idea what this is
      autofit-larger = "100%x100%";
    };
    scripts = with pkgs.mpvScripts; [
      # Allows external control of Mpv playback
      mpris
      # Auto-load playlist entires before and after the current file
      autoload
      # Provide thumbnail previews while hovering over the seekbar
      thumbnail
      # Block sponsors
      sponsorblock
    ];
  };
}
