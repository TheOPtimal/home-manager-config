_: {
  programs.skim = {
    enable = true;
    enableZshIntegration = true;
  };
}
