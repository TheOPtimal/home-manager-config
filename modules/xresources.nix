{config, ...}: {
  xresources.properties = with config.scheme.withHashtag; let
    inherit (config) fonts;
  in {
    "*.foreground" = base04;
    "*.background" = base00;

    "*.cursorColor" = base05;
    # "*fading" = 35;
    # "*fadeColor" = base03;

    "*.color0" = base00;
    "*.color1" = base08;
    "*.color2" = base0B;
    "*.color3" = base0A;
    "*.color4" = base0D;
    "*.color5" = base0E;
    "*.color6" = base0C;
    "*.color7" = base05;
    "*.color8" = base03;
    "*.color9" = base09;
    "*.color10" = base01;
    "*.color11" = base02;
    "*.color12" = base04;
    "*.color13" = base06;
    "*.color14" = base0F;
    "*.color15" = base07;

    "XTerm*renderFont" = true;
    "XTerm*faceName" = fonts.monospace.font;
    "XTerm*FaceSize" = fonts.size.body;
  };
}
