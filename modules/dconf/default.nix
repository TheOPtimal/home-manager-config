_: {
  dconf.settings = let
    wallpaper = "file://${./wallpaper.webp}";
  in {
    "org/gnome/desktop/background" = {
      picture-uri = wallpaper;
      picture-uri-dark = wallpaper;
    };
  };
}
