_: {
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Add ~/.local/bin to PATH
  home.sessionPath = [(builtins.toString ~/.local/bin)];

  # Enable HTML_based manual
  manual.html.enable = true;
}
