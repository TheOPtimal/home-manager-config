{
  pkgs,
  opkgs,
  lib,
  ...
}: rec {
  home.packages = with pkgs.gnomeExtensions; [
    blur-my-shell
    opkgs.pano
    gsconnect
    removable-drive-menu
    bluetooth-quick-connect
    sound-output-device-chooser
    coverflow-alt-tab
    panel-corners
    rounded-window-corners
    hot-edge
    just-perfection
    replace-activities-text
    tiling-assistant
  ];

  dconf.settings = with lib.hm.gvariant; {
    # Enable installed extensions
    "org/gnome/shell".enabled-extensions = map (extension: extension.extensionUuid) home.packages;

    "org/gnome/shell".disabled-extensions = [];

    # Configure blur-my-shell
    "org/gnome/shell/extensions/blur-my-shell" = {
      brightness = 0.20;
      dash-opacity = 0.25;
      sigma = 60; # Sigma means blur amount
      color = mkTuple [0.25 0.25 0.25];
      static-blur = true;
    };

    "org/gnome/shell/extensions/blur-my-shell/appfolder" = {
      blur = true;
      style-dialogs = 3;

      customize = true;
      brightness = 0.25;
      sigma = 10;
    };

    "org/gnome/shell/extensions/blur-my-shell/lockscreen" = {
      blur = true;
      brightness = 0.7;
      customize = true;
      sigma = 20;
    };

    "org/gnome/shell/extensions/blur-my-shell/panel".blur = false;

    # Configure GSConnect
    "org/gnome/shell/extensions/gsconnect".show-indicators = false;

    # Configure Pano
    "org/gnome/shell/extensions/pano" = {
      global-shortcut = ["<Super>comma"];
      incognito-shortcut = ["<Shift><Super>less"];
    };

    # Configure Panel Corners
    "org/gnome/shell/extensions/panel-corners" = {
      panel-corner-background-color = "rgb(0,0,0)";
      panel-corner-opacity = 1;
      panel-corners = true;
      screen-corners = true;
    };

    # Configure Rounded Window Corners
    "org/gnome/shell/extensions/rounded-window-corners" = {
      tweak-kitty-terminal = true;
      skip-libhandy-app = false;
      settings-version = 5;
      custom-rounded-corner-settings = ''{'Alacritty': <{'padding': <{'left': <uint32 9>, 'right': <uint32 5>, 'top': <uint32 31>, 'bottom': <uint32 5>}>, 'keep_rounded_corners': <{'maximized': <true>, 'fullscreen': <true>}>, 'border_radius': <uint32 12>, 'smoothing': <uint32 0>, 'enabled': <true>}>}'';
    };
  };
}
