_: {
  accounts.email.accounts = let
    realName = "Jacob Gogichaishvili";
  in {
    Primary = {
      address = "iakob.gogichaishvili" + "@" + "gmail.com";
      inherit realName;
      flavor = "gmail.com";
      primary = true;
    };
    Secondary = {
      address = "jggsecondary" + "@" + "gmail.com";
      inherit realName;
      flavor = "gmail.com";
    };
    School = {
      address = "iakob.gogichaishvili" + "@" + "europeanschool.ge";
      inherit realName;
      flavor = "gmail.com";
    };
  };
}
